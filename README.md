# Snake Game

New Flutter project with raywenderlich.com and clean code architecture. In which we can find a brief and concise reference to the use of clean architectures using Providers as state manager in the following Snake Game application. Briefly we will see step by step the construction of the video game project, through the article, the access will be left in the project references.

<!-- Some of the libraries used in this project are: -->

<!-- - http: ^ 0.12.0 + 4
- provider: 4.3.1
- google_fonts: any
- animate_do: ^ 1.7.3
- flutter_svg: ^ 0.19.0
- font_awesome_flutter: ^ 9.1.0 -->

<!-- ## Final project demonstration

![](/assets/markdown/demo.gif)  -->


<!-- ## Getting Started

The present examination was carried out for the ZetaGas plant company, however for the integrity of the company it decided not to carry out the same design as the original, however I believe that this examination contains even better challenges to verify the feasibility of the technology and handling. method on Flutter.

![image](/assets/markdown/pokeapi_256.png)  -->




## References

- Articulo:  https://www.raywenderlich.com/19430602-how-to-create-a-2d-snake-game-in-flutter
- Clean Code Architecture: https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html
