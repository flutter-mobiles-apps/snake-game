import 'package:flutter/material.dart';
import 'package:demo/presentation/common/colors.dart';

class Themes {
  static final themeData = ThemeData(
    primarySwatch: ColorsApp.colorBlue,
    visualDensity: VisualDensity.adaptivePlatformDensity,
  );
}
