import 'package:flutter/material.dart';
import 'package:demo/presentation/common/theme.dart';
import 'package:demo/presentation/pages/home/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Snake Game App',
      debugShowCheckedModeBanner: false,
      theme: Themes.themeData,
      home: HomePage(),
    );
  }
}
